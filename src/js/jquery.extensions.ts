/// <reference path="definitions/jquery.d.ts"/>

class JqueryExtensions {
    PreventLinking(eventObject:JQueryEventObject, ...args:any[]) {
        eventObject.preventDefault();
        console.debug('prevented');
    };
    SetEnabledState(element:any, enable:boolean) {
        var $element = $(element);
        if (!$element.is('a')) {
            element.disabled = !enable;
        }
        else {
            if(enable) {
                $element.removeClass('disabled');
               $element.off("click.disabled");
            } else {
                $element.addClass('disabled');
                $element.on("click.disabled", jqueryExtensions.PreventLinking); 
            }
        }
    }
    ToggleContainerEnabledState(senders:any) {
        var $senders = $(senders);
        $senders.each((i, container) => {
            var $container = $(container),
                disabled = $container.hasClass('disabled');
            $(container).find('a, input, button, select, textarea').each((d, element) => {
                this.SetEnabledState(element, disabled);
            });
            $container.toggleClass('disabled');
            $container.find(".container-overlay").toggleClass('hidden');                     
        });
    }
    ToggleEnabledState(sender:any, enable:boolean = null) {
        var $sender = $(sender);
        if (!$sender.is('a')) {
            enable = enable || sender.disabled;
        }
        else {
            enable = enable || $sender.hasClass('disabled');
        }
        this.SetEnabledState(sender, enable);
    }    
    SerializeFormToJson(form:HTMLFormElement) {
        var json = {};
        var value = $(form).serializeArray();
        $.each(value, function () {
            if ((<any>json)[this.name]) {
                if (!(<any>json)[this.name].push) {
                    (<any>json)[this.name] = [(<any>json)[this.name]];
                }
                (<any>json)[this.name].push(this.value || '');
            } else {
                (<any>json)[this.name] = this.value || '';
            }
        });
        return json;
    }    
}

var jqueryExtensions = new JqueryExtensions();

$.fn.serializeFormJson = function () { 
    return jqueryExtensions.SerializeFormToJson(this); 
};
$.fn.exists = function () { return this.length > 0; };
$.fn.toggleContainerEnabledState = function(){
    jqueryExtensions.ToggleContainerEnabledState(this);
};
$.fn.toggleEnabledState = function () {
    this.each(function(){
        jqueryExtensions.ToggleEnabledState(this);
    });
};