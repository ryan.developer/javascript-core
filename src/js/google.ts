/// <reference path="definitions/jquery.d.ts" />

// Example:
// var ga = new GoogleAnalytics({ propertyID: 'UA-X-1'});
// ga.RegisterClickTracker($("#element-id"), 'a');


class GoogleOptions{
    propertyID?:string
}

class GoogleAnalytics {
    Instance:any
    
    constructor(options:GoogleOptions){
        this.createScript();
        this.Instance = this.createInstance(window, 'gapi');
        this.Instance('create', options.propertyID, 'auto');
    }
    private createScript(){
        var script = document.createElement("script");
        script.async = true, script.src = 'https://www.google-analytics.com/analytics.js';
        var firstScriptElement = document.getElementsByTagName('script')[0]; 
        firstScriptElement.parentNode.insertBefore(script, firstScriptElement);
    }
    private createInstance(win:any, globalName:any){
        win['GoogleAnalyticsObject'] = globalName;
        var instance:any = win[globalName] || function () {
            (instance.q = instance.q || []).push(arguments)
        };
        instance.l = 1 * <any>new Date(); 
        return win[globalName] = instance;
    }
    SendPageView() {
        try {
            this.Instance('send', 'pageview');
        }
        catch(ex){
            console.dir(ex);
        }
    }
    SendEvent(tags:string, label:string) {
        console.debug(label);

        try {
            this.Instance('send', {
                hitType: 'event',
                eventCategory: tags,
                eventAction: 'click',
                eventLabel: label
            });
        }
        catch(ex){
            console.dir(ex);
        }
    }
    SendLink(element: JQuery) {
        var label:string = element.attr('href') || element.data('model-url') || "";
        label = label.toLowerCase().split('?')[0];
        
        var tags = element.data('tags');
        if(label === "#" || label.indexOf("javascript") > 0) {
            label = element.attr("title");
        }
        this.SendEvent(tags, label);
    }
    RegisterClickTracker(container:JQuery, selector:string, stopPropagation:boolean = false) {
        container.on('click', selector, (event) => {
            if(stopPropagation) {
                event.stopPropagation();
            }
            var sender = $(event.target);
            var id = sender.attr('id'),
                element = event.target.tagName.toLowerCase(),
                href = sender.attr('href') || sender.data('model-url') || '',
                culture = sender.data('culture'),
                tags = sender.data('tags') || (element + ' clicked');
            var label = { id: id, href: href, culture: culture, element: element };
            this.SendEvent(tags, JSON.stringify(label));
        });
    }
}