module string {
    export function AsFunction(value: string): Function {
        try {
            let scope:any = window,
                scopes = value.split('.');
            for (let i = 0; i < scopes.length; i++) {
                scope = scope[scopes[i]];
                if (scope === undefined) {
                    console.log("Function does not exist: " + value);
                    return;
                }
            }
            return <Function>scope;
        }
        catch (err) { 
            console.debug(err);
        };
    }
}