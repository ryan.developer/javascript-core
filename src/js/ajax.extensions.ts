/// <reference path="definitions/knockout.d.ts"/>
/// <reference path="definitions/bootstrap.d.ts"/>
/// <reference path="definitions/jquery.d.ts"/>

class AjaxExtensions {
    GetHtml(url:string, callback:Function, precall:Function = null, complete:Function = null) {
        $.ajax({ 
            url: url, 
            cache:false, 
            type:"GET", 
            dataType:"html", 
            beforeSend: <any>precall, 
            success: <any>callback, 
            complete: <any>complete 
        });
    }
    PostForm(form:HTMLFormElement, callback:Function, precall:Function = null, complete:Function = null) {
        var beforeSend = function(){
            if(precall) { precall(); } 
            jqueryExtensions.ToggleContainerEnabledState(form);
        };
        var action:string = form.action;
        var data = jqueryExtensions.SerializeFormToJson(form);
        $.ajax({
            url: action, 
            cache:false, 
            type:"POST", 
            data: data,
            dataType:"html", 
            beforeSend: <any>precall, 
            success: <any>callback, 
            complete: <any>complete 
        });
    }
}

var p = new AjaxExtensions();
p.PostForm(null, ()=>{}, ()=>{});