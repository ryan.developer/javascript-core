var AjaxExtensions = (function () {
    function AjaxExtensions() {
    }
    AjaxExtensions.prototype.GetHtml = function (url, callback, precall, complete) {
        if (precall === void 0) { precall = null; }
        if (complete === void 0) { complete = null; }
        $.ajax({
            url: url,
            cache: false,
            type: "GET",
            dataType: "html",
            beforeSend: precall,
            success: callback,
            complete: complete
        });
    };
    AjaxExtensions.prototype.PostForm = function (form, callback, precall, complete) {
        if (precall === void 0) { precall = null; }
        if (complete === void 0) { complete = null; }
        var beforeSend = function () {
            if (precall) {
                precall();
            }
            jqueryExtensions.ToggleContainerEnabledState(form);
        };
        var action = form.action;
        var data = jqueryExtensions.SerializeFormToJson(form);
        $.ajax({
            url: action,
            cache: false,
            type: "POST",
            data: data,
            dataType: "html",
            beforeSend: precall,
            success: callback,
            complete: complete
        });
    };
    return AjaxExtensions;
}());
var p = new AjaxExtensions();
p.PostForm(null, function () { }, function () { });
var GoogleOptions = (function () {
    function GoogleOptions() {
    }
    return GoogleOptions;
}());
var GoogleAnalytics = (function () {
    function GoogleAnalytics(options) {
        this.createScript();
        this.Instance = this.createInstance(window, 'gapi');
        this.Instance('create', options.propertyID, 'auto');
    }
    GoogleAnalytics.prototype.createScript = function () {
        var script = document.createElement("script");
        script.async = true, script.src = 'https://www.google-analytics.com/analytics.js';
        var firstScriptElement = document.getElementsByTagName('script')[0];
        firstScriptElement.parentNode.insertBefore(script, firstScriptElement);
    };
    GoogleAnalytics.prototype.createInstance = function (win, globalName) {
        win['GoogleAnalyticsObject'] = globalName;
        var instance = win[globalName] || function () {
            (instance.q = instance.q || []).push(arguments);
        };
        instance.l = 1 * new Date();
        return win[globalName] = instance;
    };
    GoogleAnalytics.prototype.SendPageView = function () {
        try {
            this.Instance('send', 'pageview');
        }
        catch (ex) {
            console.dir(ex);
        }
    };
    GoogleAnalytics.prototype.SendEvent = function (tags, label) {
        console.debug(label);
        try {
            this.Instance('send', {
                hitType: 'event',
                eventCategory: tags,
                eventAction: 'click',
                eventLabel: label
            });
        }
        catch (ex) {
            console.dir(ex);
        }
    };
    GoogleAnalytics.prototype.SendLink = function (element) {
        var label = element.attr('href') || element.data('model-url') || "";
        label = label.toLowerCase().split('?')[0];
        var tags = element.data('tags');
        if (label === "#" || label.indexOf("javascript") > 0) {
            label = element.attr("title");
        }
        this.SendEvent(tags, label);
    };
    GoogleAnalytics.prototype.RegisterClickTracker = function (container, selector, stopPropagation) {
        var _this = this;
        if (stopPropagation === void 0) { stopPropagation = false; }
        container.on('click', selector, function (event) {
            if (stopPropagation) {
                event.stopPropagation();
            }
            var sender = $(event.target);
            var id = sender.attr('id'), element = event.target.tagName.toLowerCase(), href = sender.attr('href') || sender.data('model-url') || '', culture = sender.data('culture'), tags = sender.data('tags') || (element + ' clicked');
            var label = { id: id, href: href, culture: culture, element: element };
            _this.SendEvent(tags, JSON.stringify(label));
        });
    };
    return GoogleAnalytics;
}());
var JqueryExtensions = (function () {
    function JqueryExtensions() {
    }
    JqueryExtensions.prototype.PreventLinking = function (eventObject) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        eventObject.preventDefault();
        console.debug('prevented');
    };
    ;
    JqueryExtensions.prototype.SetEnabledState = function (element, enable) {
        var $element = $(element);
        if (!$element.is('a')) {
            element.disabled = !enable;
        }
        else {
            if (enable) {
                $element.removeClass('disabled');
                $element.off("click.disabled");
            }
            else {
                $element.addClass('disabled');
                $element.on("click.disabled", jqueryExtensions.PreventLinking);
            }
        }
    };
    JqueryExtensions.prototype.ToggleContainerEnabledState = function (senders) {
        var _this = this;
        var $senders = $(senders);
        $senders.each(function (i, container) {
            var $container = $(container), disabled = $container.hasClass('disabled');
            $(container).find('a, input, button, select, textarea').each(function (d, element) {
                _this.SetEnabledState(element, disabled);
            });
            $container.toggleClass('disabled');
            $container.find(".container-overlay").toggleClass('hidden');
        });
    };
    JqueryExtensions.prototype.ToggleEnabledState = function (sender, enable) {
        if (enable === void 0) { enable = null; }
        var $sender = $(sender);
        if (!$sender.is('a')) {
            enable = enable || sender.disabled;
        }
        else {
            enable = enable || $sender.hasClass('disabled');
        }
        this.SetEnabledState(sender, enable);
    };
    JqueryExtensions.prototype.SerializeFormToJson = function (form) {
        var json = {};
        var value = $(form).serializeArray();
        $.each(value, function () {
            if (json[this.name]) {
                if (!json[this.name].push) {
                    json[this.name] = [json[this.name]];
                }
                json[this.name].push(this.value || '');
            }
            else {
                json[this.name] = this.value || '';
            }
        });
        return json;
    };
    return JqueryExtensions;
}());
var jqueryExtensions = new JqueryExtensions();
$.fn.serializeFormJson = function () {
    return jqueryExtensions.SerializeFormToJson(this);
};
$.fn.exists = function () { return this.length > 0; };
$.fn.toggleContainerEnabledState = function () {
    jqueryExtensions.ToggleContainerEnabledState(this);
};
$.fn.toggleEnabledState = function () {
    this.each(function () {
        jqueryExtensions.ToggleEnabledState(this);
    });
};
var string;
(function (string) {
    function AsFunction(value) {
        try {
            var scope = window, scopes = value.split('.');
            for (var i = 0; i < scopes.length; i++) {
                scope = scope[scopes[i]];
                if (scope === undefined) {
                    console.log("Function does not exist: " + value);
                    return;
                }
            }
            return scope;
        }
        catch (err) {
            console.debug(err);
        }
        ;
    }
    string.AsFunction = AsFunction;
})(string || (string = {}));
//# sourceMappingURL=core.js.map