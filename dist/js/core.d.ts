/// <reference path="../../src/js/definitions/knockout.d.ts" />
/// <reference path="../../src/js/definitions/bootstrap.d.ts" />
/// <reference path="../../src/js/definitions/jquery.d.ts" />
declare class AjaxExtensions {
    GetHtml(url: string, callback: Function, precall?: Function, complete?: Function): void;
    PostForm(form: HTMLFormElement, callback: Function, precall?: Function, complete?: Function): void;
}
declare var p: AjaxExtensions;
declare class GoogleOptions {
    propertyID?: string;
}
declare class GoogleAnalytics {
    Instance: any;
    constructor(options: GoogleOptions);
    private createScript();
    private createInstance(win, globalName);
    SendPageView(): void;
    SendEvent(tags: string, label: string): void;
    SendLink(element: JQuery): void;
    RegisterClickTracker(container: JQuery, selector: string, stopPropagation?: boolean): void;
}
declare class JqueryExtensions {
    PreventLinking(eventObject: JQueryEventObject, ...args: any[]): void;
    SetEnabledState(element: any, enable: boolean): void;
    ToggleContainerEnabledState(senders: any): void;
    ToggleEnabledState(sender: any, enable?: boolean): void;
    SerializeFormToJson(form: HTMLFormElement): {};
}
declare var jqueryExtensions: JqueryExtensions;
declare module string {
    function AsFunction(value: string): Function;
}
